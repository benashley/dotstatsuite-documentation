Powered by [Hugo](https://gohugo.io/) website using GitLab Pages.<br>
The theme used is https://themes.gohugo.io/hugo-theme-techdoc/ and adapted from http://themes.gohugo.io/beautifulhugo/.<br>

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

### Purpose

This project is used for publishing the official .Stat Suite documentation to the public Gitlab webpage https://sis-cc.gitlab.io/dotstatsuite-documentation/.

---

### Table of Contents

```
.
├── content
│   ├── about
│   │   ├── _index.md
│   │   ├── code of conduct.md
│   │   ├── dotstat-core-nuget-dependency-licenses.md
│   │   ├── license.md
│   │   ├── product overview.md
│   ├── changelog
│   │   ├── _index.md
│   ├── cms-integration
│   │   ├── _index.md
│   │   ├── ckan.md
│   │   ├── drupal.md
│   │   ├── worpress.md
│   ├── configurations
│   │   ├── _index.md
│   │   ├── de-configuration.md
│   │   ├── de-customisation.md
│   │   ├── solr-optimisation.md
│   ├── contribution
│   │   ├── _index.md
│   │   ├── development guidelines.md
│   │   ├── issue process.md
│   │   ├── report an issue.md
│   │   ├── team members.md
│   ├── getting started
│   │   ├── _index.md
│   │   ├── devops.md
│   │   ├── framework.md
│   │   ├── infrastructure-requirements.md
│   │   ├── semantic-version.md
│   ├── images
|   |   ├── fonts
|   |   ├── ...
│   ├── install-docker
│   │   ├── _index.md
│   │   ├── as-container.md
│   ├── install-service
│   │   ├── _index.md
│   │   ├── as-a-service.md
│   ├── install-source-code
│   │   ├── _index.md
│   │   ├── monotenant-install-from-artifacts.md
│   │   ├── monotenant-install-js-as-windows-services.md
│   │   ├── monotenant-update-from-artifacts.md
│   │   ├── source-code.md
│   │   ├── windows-stat-core-services.md
│   ├── _index.md
├── dotstatsuitejs
|   ├── artefacts-sh
|   ├── pm2.sh
├── static
|   ├── favicon.ico
├── themes
|   ├── techdoc
│   │   ├── ...
.gitignore
.gitlab-ci.yml
LICENSE
MANIFEST.md
README.md
config.toml
```

### Contributing to the documentation

- All the documentation content is written in **Markdown** syntax.
- If you want to **edit the documentation** web page, fork [this project](https://gitlab.com/sis-cc/dotstatsuite-documentation), make your changes there, and submit a merge request.
- Please follow the existing **menu structure**. If relevant, you can submit a new menu folder/section/sub-section.
- Your merge request will be reviewed according to our **[Gitflow](https://sis-cc.gitlab.io/dotstatsuite-documentation/contribution/development-guidelines/) process**.
