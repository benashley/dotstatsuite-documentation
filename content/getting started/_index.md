---
title: "Getting started"
subtitle: 
comments: false
weight: 20

---

includes: 

* [.Stat Suite Open Source framework](/getting-started/framework)
* [DevOps implementation](/getting-started/devops)
* [Semantic versioning of .Stat Suite CORE components and services](/getting-started/semantic-version)
* [Infrastructure requirements](/getting-started/infrastructure-requirements)

### Delivery and support streams' diagram

![DevOps diagram](/images/devops-schema.png)

In this diagram are represented the deliverables we provide (in green) based on the 3 different delivery approaches that we support.  

In blue are represented the areas where some technical support can be provided by third-parties' technical partner(s) with the following tasks:  
* Advanced support for source code installations for technical experts (in collaboration with the Developer advocate);
* Support for source code installations for technical newbies (creation of simple installation scripts, hand-holding);
* Advanced support for container based installations (using Docker images) on premise (onsite/own cloud) for technical experts;
* Software as a Service (SaaS): host pilot, test of production instances.
