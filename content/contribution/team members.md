---
title: "Team members"
subtitle: 
comments: false
weight: 94
---

#### .Stat Suite developers

- Aleksei Arhipov [@Aleksei-oecd](https://gitlab.com/aleksei-oecd)
- Balazs Szilagyi [@balazs82](https://gitlab.com/balazs82)
- Dimitri Roncoli [@RedPDRoncoli](https://gitlab.com/RedPDRoncoli)
- Marco Oksman [@moksman](https://gitlab.com/moksman)
- Mike Velluet [@mike.velluet](https://gitlab.com/mike.velluet)
- Nicolas Briemant [@nicolas-briemant](https://gitlab.com/nicolas-briemant)
- Pedro Carranza [@pedroacarranza](https://gitlab.com/pedroacarranza)
- Zsolt Lenart [@ZsoltLenart](https://gitlab.com/ZsoltLenart)

#### .Stat Suite developer advocate

- Ben Ashley [@benashley](https://gitlab.com/benashley)

#### .Stat Suite product owners

- Jean-Baptiste Nonin [@j3an-baptiste](https://gitlab.com/j3an-baptiste)
- Jens Dossé [@dosse](https://gitlab.com/dosse)
- Sandrine Phelipot-Souflis [@SSandrine](https://gitlab.com/SSandrine)


#### .Stat Suite graphical designer

- Orsolya Zajtai [@zajtaiorsolya](https://gitlab.com/zajtaiorsolya)


#### .Stat Suite community manager
- Jonathan Challener [@ChallenerSJ](https://gitlab.com/ChallenerSJ)
