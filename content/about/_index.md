---
title: "About"
subtitle: 
comments: false
weight: 10

---

includes:

* [License](/about/license)
* [Product overview and flight planner](/about/product-overview)
* [Code of Conduct](/about/code-of-conduct)
* [Licenses of NuGet dependencies of .Stat Core module](/about/dotstat-core-nuget-dependency-licenses)