---
title: "Customise data views"
subtitle: 
comments: false
weight: 330

---

Further in these sub-sections are explained how one can customise a data view with the use of standardised SDMX Annotations and/or the business rules that illsutrate many statistical disseminations' use cases.

* [Default layout](/using-dlm/custom-data-view/default-layout)
* [Default filter selections](/using-dlm/custom-data-view/default-selection)
* [Hide information of a data view](/using-dlm/custom-data-view/not-displayed)
* [Implicit and explicit orders](/using-dlm/custom-data-view/implicit-explicit-order)
* [Additional downloads of external resources](/using-dlm/custom-data-view/external-resources)

