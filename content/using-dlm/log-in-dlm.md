---
title: "Log in the DLM"
subtitle: 
comments: false
weight: 210

---

<!-- 
ToC
- [Log in](#log-in)
- [Log out](#log-out)
 -->

### Log in
*under construction...*

### Log out
>Released in [February 28, 2020 Release .Stat Suite JS 4.0.0](https://sis-cc.gitlab.io/dotstatsuite-documentation/changelog/#february-28-2020)

Once logged in the DLM, a **Log out** feature is available in the common header of the interface.  

![DLM log out](/using-dlm/files/dlm-log-out.png)

Under the username icon, the pop-up window displays his email address (used for credential), and the "Log out" button.  
When the user clicks on "Log out", then he is unregistered from .Stat DLM and redirected to the Keycloak Log in page.
